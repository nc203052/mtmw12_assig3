import numpy as np

# Functions for calculating gradients

def gradient_2point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance
    dx apart using 2-point differences. Returns an array the same size as f"""

    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    # Two point differences at the end points
    dfdx[0] = (f[1] - f[0])/dx
    dfdx[-1] = (f[-1] - f[-2])/dx
    # Centred differences for the mid-points
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(2*dx)
    return dfdx

def gradient_2ndOrder(f, dx):
    """The gradient of one dimensional array f assuming points are a distance
     dx apart calculated to 2nd order accuracy. Returns an array the same
    size as f"""

    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    # Second order gradients at the end points
    dfdx[0] = (-3*f[0] + 4*f[1] - f[2])/(2*dx)
    dfdx[-1] = (f[-3] - 4*f[-2] + 3*f[-1])/(2*dx)
    # Centred differences for the mid-points
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(2*dx)
    return dfdx

def gradient_midPoint(f, dx):
    """The gradient of one dimensional array f at the mid-points between the
       values of f assuming points are a distance dx apart. Returns an arry
       of size one less than f."""

    # Initialise the gradient to be smaller than f
    dfdx = np.zeros(len(f)-1)
    # Gradients at mid points
    for i in range(len(dfdx)):
        dfdx[i] = (f[i+1] - f[i])/dx
    return dfdx
