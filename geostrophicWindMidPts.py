# MTMW12 assignment 3. Jonathan O'Rayne 13th November 2021
# Python3 code to numerically differentiate the pressure in order to calculate
# the geostrphic wind.
# Calculations at mid-points and compare with co-located calculation

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def geostrophicWindMidPts():
    """Calculate the geostrophic wind via mid-point differences.
        Plot analytical and numerical solutions and graph the errors"""

    # Resolution and size of the domain
    N = 10               # the number of intervals to divide space into
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin)/N # the length of the spacing

    # The spatial dimension, y:
    y2point = np.linspace(ymin, ymax, N+1)
    yMidPts = np.linspace(ymin + dy/2,ymax - dy/2, N)  #y w.r.t. mid points

    # The pressure at the y points and the exact geostrophic wind
    p = pressure(y2point, physProps)
    uExact_2point = uGeoExact(y2point, physProps)
    uExact_MidPts = uGeoExact(yMidPts, physProps)

    # The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy, physProps)

    # Using higher order differential to find a more accurate
    #    numerical solution for the geostrophic wind
    u_3point = geoWind(gradient_2ndOrder(p, dy), physProps)
    u_3point_error = u_3point - uExact_2point

    # Using mid-points differential to find a more accurate solution with
    # same number of points, N.
    # The pressure gradient and wind using mid-point differences
    dpdy_MidPts = gradient_midPoint(p, dy)
    u_MidPts = geoWind(dpdy_MidPts, physProps)

    # Calculate Errors
    u_2point_error = u_2point - uExact_2point
    u_MidPts_error = u_MidPts - uExact_MidPts

    # Graph to compare the numerical and analytic solutions
    # plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)

    # Plot the approximate and exact wind at y points
    plt.plot(y2point/1000, uExact_2point, 'k-', label='Exact')
    plt.plot(y2point/1000, u_3point, 'xk-.', label='2nd order differences', \
               ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('fig-2-1_geoWind3pt.pdf')
    plt.show()
    plt.clf()

    # plot the errors
    plt.plot(y2point/1000, u_3point_error, 'xk-.', \
             label='2nd order differences', ms=12, \
             markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.axhline(linestyle='-', color='k')
    plt.xlabel('y (km)')
    plt.ylabel('error in u (m/s)')
    plt.tight_layout()
    plt.savefig('fig-2-2_geoWindErrors3pt.pdf')
    plt.show()
    plt.clf()

    # Plot the approximate and exact wind at y mid-points
    plt.plot(yMidPts/1000, uExact_MidPts, 'k-', label='Exact')
    plt.plot(yMidPts/1000, u_MidPts, '*k--', label='Mid-point differences', \
               ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('fig-3-1_geoWindMidPts.pdf')
    plt.show()
    plt.clf()

    # plot the errors at the mid-points
    plt.plot(yMidPts/1000, u_MidPts_error, '*k--', \
             label='Mid-point differences', \
             ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.axhline(linestyle='-', color='k')
    plt.xlabel('y (km)')
    plt.ylabel('error in u (m/s)')
    plt.tight_layout()
    plt.savefig('fig-3-2_geoWindErrorsCent.pdf')
    plt.show()
    plt.clf()

    # Compare errors between 2-point differences, mid-point differences &
    # 2nd order differences.
    plt.plot(y2point/1000, u_2point_error, 'ok-', \
             label='2-point differences', ms=12, \
             markeredgewidth=1.5, markerfacecolor='none')
    plt.plot(yMidPts/1000, u_MidPts_error, '*k--', \
             label='Mid-point differences', ms=12, \
             markeredgewidth=1.5, markerfacecolor='none')
    plt.plot(y2point/1000, u_3point_error, 'xk--', \
             label='2nd order differences', ms=12, \
             markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.axhline(linestyle='-', color='k')
    plt.xlabel('y (km)')
    plt.ylabel('error in u (m/s)')
    plt.tight_layout()
    plt.savefig('fig-4-1_geoWindErrorsComparison.pdf')
    plt.show()
    plt.clf()

geostrophicWindMidPts()
